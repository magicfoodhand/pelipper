const chai = require('chai')
chai.should();

const { Pelipper } = require('../src/pelipper')

describe('Pelipper', () => {
    it('can deliver a message', () => {
        let emailsSent = 0
        Pelipper().register('email', ({message}) => {
            message.should.equal('test')
            emailsSent++
        }).deliver({ type: 'email', message: 'test' })
        emailsSent.should.equal(1)
    })

    it('can deliver multiple messages', () => {
        let emailsSent = 0
        Pelipper().register('email', ({message}) => {
            message.should.equal('test')
            emailsSent++
        }).deliver(
            { type: 'email', message: 'test' }, 
            { type: 'email', message: 'test' }
        )
        emailsSent.should.equal(2)
    })


    it('can deliver a message of any type', () => {
        let emailsSent = 0
        Pelipper().register('email', ({message}) => {
            message.contents.should.equal('test')
            emailsSent++
        }).deliver({ type: 'email', message: {
            from: 'info@example.com',
            to: 'tester@example.com',
            subject: 'testing',
            contents: 'test'
        } })
        emailsSent.should.equal(1)
    })

    it('can recieve config', () => {
        let emailsSent = 0
        let config = { meaningOfLife: 42 }
        Pelipper({ config }).register('email', ({message}, _config) => {
            _config.should.equal(config)
            emailsSent ++
        }).deliver({ type: 'email', message: 'test' })
        emailsSent.should.equal(1)
    })

    describe('#register', () => {
        it('does not override existing values', () => {
            let emailsSent = 0
            let secondEmailsSent = 0
            Pelipper().register('email', () => {
                emailsSent += 1
            }).register('email', () => {
                secondEmailsSent += 1
            }).deliver({ type: 'email', message: 'test' })
            emailsSent.should.equal(1)
            secondEmailsSent.should.equal(1)
        })
    })

    it('ignored unknown types', () => {
        let emailsSent = 0
        Pelipper().register('email', () => {
            emailsSent++
        }).deliver({ type: 'text', message: 'test' })
        emailsSent.should.equal(0)
    })

    it('can run multiple', () => {
        let emailsSent = 0
        let textsSent = 0
        Pelipper().register('email', ({message}) => {
            message.should.equal('test')
            emailsSent++
        }).register('text', ({message}) => {
            message.should.equal('test')
            textsSent++
        }).deliver({ type: ['email', 'text'], message: 'test' })
        emailsSent.should.equal(1)
        textsSent.should.equal(1)
    })

    describe('__all', () => {
        it('can run all', () => {
            let emailsSent = 0
            let textsSent = 0
            Pelipper().register('email', ({message}) => {
                message.should.equal('test')
                emailsSent++
            }).register('text', ({message}) => {
                message.should.equal('test')
                textsSent++
            }).deliver({ type: '__all', message: 'test' })
            emailsSent.should.equal(1)
            textsSent.should.equal(1)
        })

        it('can be disabled', () => {
            let emailsSent = 0
            let textsSent = 0
            Pelipper({ allowAll: false }).register('email', ({message}) => {
                message.should.equal('test')
                emailsSent++
            }).register('text', ({message}) => {
                message.should.equal('test')
                textsSent++
            }).deliver({ type: '__all', message: 'test' })
            emailsSent.should.equal(0)
            textsSent.should.equal(0)
        })
    })
})