const { Limitless } = require('limitlessjs')

const matchesRunType = (runType, { type }) =>
    type === '__all' || matchesRunTypeExactly(runType, { type, })

const matchesRunTypeExactly = (runType, { type }) =>
     type === runType || Array.isArray(type) && type.includes(runType)

const adaptHandler = (eventHandler) => 
    (message, _job, _name, _pastResults, _event, config) => 
        eventHandler(message, config)

module.exports.Pelipper = ({ 
    allowAll = true, config = {},
} = {}) => {
    const { 
        process, withJobDefinition, withRunHandler, withTriggerHandler, 
    } = Limitless({ config, })

    withTriggerHandler('matchesRunType', matchesRunType)
    withTriggerHandler('matchesRunTypeExactly', matchesRunTypeExactly)

    const core = {
        deliver: process,
        register: (name, handler) => {
            const runType = Math.random().toString(36)
            withJobDefinition({ runType, triggers: [{
                type: allowAll ? 'matchesRunType' : 'matchesRunTypeExactly',
                definition: name,
            },], })
            withRunHandler(runType, adaptHandler(handler))
            return core
        },
    }
    return core
}