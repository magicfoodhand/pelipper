[![pipeline status](https://gitlab.com/inapinch/pelipper/badges/master/pipeline.svg)](https://gitlab.com/inapinch/pelipper/commits/master)
[![coverage report](https://gitlab.com/inapinch/pelipper/badges/master/coverage.svg)](https://gitlab.com/inapinch/pelipper/commits/master)

# Pelipper
A small (707 Bytes) extension over [Limitless](https://www.npmjs.com/package/limitlessjs), deliver messages anywhere.

![Pelipper (Pokemon) Delivering Mail](https://archives.bulbagarden.net/media/upload/1/17/SOS_Mail_PSMD.png)

## Installation
```yarn add pelipper```

```npm install --save pelipper```

## Usage
 
    const { Pelipper } = require('pelipper')
    
    Pelipper()
        .register('email', ({ message }) => {
            // send message
            console.log(message)
            // return something for further processing
            return message
        })
        .deliver({ type: 'email', message: 'Hello World' })
        .filter(message => message === 'Hello World')
